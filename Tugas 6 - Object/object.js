//SanbercodeIMRN0620
//created by budi.gutama 20200623
//tugas 6 - Object

//No. 1 
function arrayToObject(arr) {
    peopleObj={};
    var now = new Date()
    var thisYear = now.getFullYear() 

    n=1;
    arr.forEach(function(item){
        var umur = "invalid Brith Year";
        if (item[3]<=thisYear) {umur=thisYear-item[3];}
        peopleObj = {
            firstName: item[0],
            lastName: item[1],
            gender: item[2],
            age: umur
       } 
       console.log(n+'. '+item[0]+' '+item[1]+' : '); console.log(peopleObj);
       n++;
    }); 

}
 
console.log('Soal No. 1')  
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([]) // ""

//No 2.
var barangObj={
    'Sepatu Stacattu': 1500000
    , 'Baju Zoro': 500000
    , 'Baju H&N': 250000
    , 'Sweater Uniklooh': 175000
    , 'Casing Handphone': 50000
};

function shoppingTime(memberId, money) {
    var belanjaObj={};
    var minimal_uang=50000;
    if (!memberId || memberId==""){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }
    if (money<minimal_uang){
        return 'Mohon maaf, uang tidak cukup';
    }
    let entries = Object.entries(barangObj);
    let sorted = entries.sort((a, b) => b[1] - a[1]);
    belanjaObj.memberId = memberId;
    belanjaObj.money = money;

    //console.log(sorted);
    var sisa_uang = money;
    list_barang=[];
    sorted.forEach(function(item){
        if (item[1]<=sisa_uang){
            list_barang.push(item[0]);
            sisa_uang -= item[1];
        } 
    }); 
    //console.log(list_barang);
    belanjaObj.listPurchased = list_barang;
    belanjaObj.changeMoney = sisa_uang;

    return belanjaObj;
}

console.log('\n\n Soal No. 2')   
  // TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
  

//No 2.

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    lsPenumpang=[];
    var ongkos =2000;
    arrPenumpang.forEach(function(ap){
        var n = 0;
        var mulai=0;var selesai=0;
        for (let ar of rute){
            if(ap[1]==ar){
                mulai = n;
            }
            if(ap[2]==ar){
                selesai = n;
                break;
            }
            n++;
        }
        penumpangObj = {
            penumpang : ap[0],
            naikDari : ap[1],
            tujuan : ap[2],
            bayar : ongkos*(selesai-mulai)
        }
        lsPenumpang.push(penumpangObj);
    });

    return lsPenumpang;
}

console.log('\n\n Soal No. 3')   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]