//sanbercodeIMRN2006
//created by budi.gutama 20200616
//tugas 2 conditional : if - else

var nama  = "Glory";
var peran = "Penyihir";

function ifelseWarewolf(nama, peran){
	console.log('Tugas 2 Conditional : if - else...');
	console.log('Nama 	:'+nama);
	console.log('Peran 	:'+peran);

	if (nama==""){
		console.log('Nama Harus diisi');
	}
	else {
		if (peran==""){
			console.log('Hallo '+nama+', pilih peranmu untuk memulai game!');
		}
		else {
			var lw_peran = peran.toLowerCase().trim();
			console.log('Selamat datang di Dunia Werewolf, '+nama);
			if(lw_peran=='penyihir'){
				console.log('Halo '+lw_peran+' '+nama+', kamu dapat melihat siapa yang menjadi werewolf!');
			}
			if(lw_peran=='guard'){
				console.log('Halo '+lw_peran+' '+nama+', kamu akan membantu melindungi temanmu dari serangan werewolf.');
			}
			if(lw_peran=='warewolf'){
				console.log('Halo '+lw_peran+' '+nama+', Kamu akan memakan mangsa setiap malam!');
			}
		}
	}
}

ifelseWarewolf("Jane", "");

ifelseWarewolf("Jane", "penyihir");

console.log('\n\n')
//tugas 2 conditional : Switch Case

var tanggal =1;
var bulan =6;
var tahun =1988;

console.log('Tugas 2 Conditional : Switch case...')
console.log('Hari '+tanggal);
console.log('Bulan '+bulan);
console.log('tahun '+tahun);

isErr=false;
if (tanggal<1 || tanggal>31){
	console.log('tanggal '+tanggal+' tidak valid, harus diisi angka 1 s.d. 31');
	isErr=true;
}
if (bulan<1 || bulan>12){
	console.log('bulan '+bulan+' tidak valid, harus diisi angka 1 s.d. 12');
	isErr=true;
}
if (tahun<1900 || tahun>2200){
	console.log('tahun '+tahun+' tidak valid, harus diisi angka 1900 s.d. 2200');
	isErr=true;
}

if (!isErr){
	switch(bulan) {
		case 1: { nama_bulan='Januari'; break; }
		case 2: { nama_bulan='Februari'; break; }
		case 3: { nama_bulan='Maret'; break; }
		case 4: { nama_bulan='April'; break; }
		case 5: { nama_bulan='Mei'; break; }
		case 6: { nama_bulan='Juni'; break; }
		case 7: { nama_bulan='Juli'; break; }
		case 8: { nama_bulan='Agustus'; break; }
		case 9: { nama_bulan='September'; break; }
		case 10: { nama_bulan='Oktober'; break; }
		case 11: { nama_bulan='November'; break; }
		case 12: { nama_bulan='Desember'; break; }
		default: { nama_bulan='bulan tidak valid!'; break; }
	}

	console.log(tanggal+' '+nama_bulan+' '+tahun);
}