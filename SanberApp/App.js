import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

//import Component from './Latihan/Component/component';
//import Router from './Tugas/Tugas13/Router';
//import Tugas14 from './Tugas/Tugas14/Router';
//import Tugas15 from './Tugas/Tugas15/index';
//import NavScreen from './Tugas/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz3/index';

export default function App() {
  return (
    <Quiz3 />
    // <View style={styles.container}>
    //   <Text>
    //     Hallo welcome to SanberApps
    //     tugas 10 IMRN lanjut
    //   </Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
