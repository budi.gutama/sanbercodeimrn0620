import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar ,
  TouchableOpacity,
  Image
} from 'react-native';

import Logo from './Asset/Logo';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Actions} from 'react-native-router-flux';

export default class Signup extends Component<{}> {

  goBack() {
      Actions.pop();
  }

	render() {
		return(
			<View style={styles.container}>
				{/* <Logo/> */}
				<View style={styles.AboutMeCont}>
					<Text style={styles.Header2}>About Me</Text>
				</View>
				<View style={styles.AboutMeCont}>
                    <Image  style={{width:280, height: 300}}
                        source={require('./Images/g2_1.jpg')}/>
                </View>
                <View>
					<Text style={styles.TextBody}>Glory Gemilang</Text>
                </View>
				<View style={styles.AboutMeCont}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Icon style={styles.navItem} name="backspace" size={35}/>
                    </TouchableOpacity>
				</View>
			</View>	
			)
	}
}

const styles = StyleSheet.create({
  container : {
    backgroundColor:'#302F36',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  AboutMeCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  Header2: {
  	color:'rgba(255,255,255,0.6)',
  	fontSize:32
  },
  TextBody: {
    color:'rgba(255,255,255,0.6)',
    marginVertical: 15,
    fontSize:18,
},
  signupButton: {
  	color:'#ffffff',
  	fontSize:16,
  	fontWeight:'500'
  }
});
