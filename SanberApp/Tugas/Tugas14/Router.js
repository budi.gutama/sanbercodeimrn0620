import React, { Component } from 'react';

import {Router, Stack, Scene} from 'react-native-router-flux';


import ToDoApp from './App';
import Skills from './SkillScreen';

export default class Routes extends Component<{}> {
	render() {
		return(
			<Router>
			    <Stack key="root" hideNavBar={true}>
			      <Scene key="ToDoApp" component={ToDoApp} title="ToDoApp"/>
			      <Scene key="Skills" component={Skills} title="Skill Screen" initial={true}/>
			    </Stack>
			 </Router>
			)
	}
}