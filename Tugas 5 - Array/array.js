//SanbercodeIMRN0620
//created by budi.gutama 20200619
//tugas 5 - Array

//No. 1 Range

//fungsi range(start num, finish num)
/*
membuat deret angkan dari stsrtNum sampe finishNum
jika start < finish maka deret angka ascending, 
jika sebaliknya maka deret angka descending
jika tidak lengkap maka return -1
*/
function range(startNum, finishNum){
    if (!finishNum){ return -1}

    r_array=[];
    if (startNum < finishNum){
        for (let n=startNum; n<=finishNum; n++){
            r_array.push(n);
        }
    }
    else{
        for (n=startNum; n>=finishNum; n--){
            r_array.push(n);
        }
    }
    return r_array;
}

console.log('SOAL No. 1');
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log('\n\n');


//No. 2 Range with step

//fungsi rangewithstep(startNum, finishNum, step)
/*
membuat deret angka dengan kelipatan dengan gap/step tertentu
*/
function rangeWithStep(startNum, finishNum, step=1){
    if (!finishNum){ return -1}

    r_array=[];
    if (startNum < finishNum){
        for (n=startNum; n<=finishNum; n+=step){
            r_array.push(n);
        }
    }
    else{
        for (n=startNum; n>=finishNum; n-=step){
            r_array.push(n);
        }
    }
    return r_array;

}

console.log('SOAL No. 2');
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log('\n\n');


//No. 3 sum of range

//fungsi sumOfRange(startNum, finishNum, step=1)
/*
membuat fungsi penjumlahan dari deret angka dengan kelipatan dengan gap/step tertentu
*/
function sum(startNum, finishNum, step=1){
    //jika tidak ada parameter yg dikirim isi 0 
    if(!startNum){return 0;}
    //jika hanya 1 parameter yg dikirim return parameter tersebut
    if(!finishNum){return startNum;}
    
    array_data = rangeWithStep(startNum, finishNum, step);
    sum_array = array_data.reduce(function(a, b){
        return a + b;
    }, 0);

    return sum_array;
}

console.log('SOAL No. 3');
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 
console.log('\n\n');


//No. 4 (Array Multidimensi)

//fungsi handling data / parsing data array multidimensi
function dataHandling(){
    input.forEach(function(item){
        console.log('Nomor ID   : '+item[0]);
        console.log('Nama       : '+item[1]);
        console.log('T.T.L.     : '+item[2]+' '+item[3]);
        console.log('Hobi       : '+item[4]);
        console.log('\n');
    });
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log('SOAL No. 4');
dataHandling();

//No. 5 (Balik Kata)
function balikKata(kata){
    ln = kata.length;
    akat="";
    for (let i=0; i<ln; ln--){
        akat = akat.concat(kata[ln-1]);
    }
    return akat;
}

console.log('SOAL No. 3');
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 
console.log('\n\n');


//No. 6 (Metode Array)
function getBuan(bln){
    let bulan=Number(bln);
	switch(bulan) {
		case 1: { nama_bulan='Januari'; break; }
		case 2: { nama_bulan='Februari'; break; }
		case 3: { nama_bulan='Maret'; break; }
		case 4: { nama_bulan='April'; break; }
		case 5: { nama_bulan='Mei'; break; }
		case 6: { nama_bulan='Juni'; break; }
		case 7: { nama_bulan='Juli'; break; }
		case 8: { nama_bulan='Agustus'; break; }
		case 9: { nama_bulan='September'; break; }
		case 10: { nama_bulan='Oktober'; break; }
		case 11: { nama_bulan='November'; break; }
		case 12: { nama_bulan='Desember'; break; }
		default: { nama_bulan='bulan tidak valid!'; break; }
    }
    return nama_bulan;
}

function dataHandling2(input){
    let new_input = input;
    new_input.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    new_input.pop();
    new_input.push("Pria", "SMA Internasional Metro")
    console.log(new_input);

    let arr_tgl = new_input[3].split("/");
    console.log(getBuan(arr_tgl[1]));
    let new_tgl = arr_tgl.join("-");
    let sort_tgl = arr_tgl.sort(function (a, b) { return b - a } );
    console.log(sort_tgl);
    console.log(new_tgl);

    nama=new_input[1].split(" ").slice(0,2).join(" ");
    console.log(nama);
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);

