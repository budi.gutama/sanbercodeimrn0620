//SanbercodeIMRN0620
//created by budi.gutama 20200617
//tugas 4 - Function

//No. 1 
function teriak(){return "Halo Sanbers!"}

console.log('No. 1');
console.log(teriak());

console.log('\n\n')

//No. 2
function kalikan (n1, n2){
    return n1 * n2
}

var num1 = 12;
var num2 = 4;

var hasilkali = kalikan(num1, num2);

console.log('No. 2')
console.log(hasilkali);

console.log('\n\n')

//No. 3
function introduce(name, age, address, hobby){
    sentence = "Nama saya "+name+", Umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby;
    return sentence;
}

var name = "Budi"
var age = 30
var address = "Jln. Prabuanbulan, Kabupaten Bandung"
var hobby = "Gaming"
 
console.log('No. 3')
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);

