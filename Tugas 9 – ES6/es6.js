//sanbercodeIMRN2006
//created by budi.gutama 20200625
//tugas 9 ES6 

//Soal No. 1
const golden = () =>{
    console.log("this is golden!!")
  }
   
  golden()


//Soal No. 2
const newFunction = (firstName, lastName) =>{
    return {
      firstName,
      lastName,
      fullName: function(){
        //console.log(firstName + " " + lastName)
        fName = `${firstName} ${lastName}`;
        console.log(fName);
        return 
      }
    }
  }
;

newFunction("William", "Imoh").fullName() ;


//Soal No.3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const{firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation);


//Soal No. 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//ES6 ways
const combined2 = [west, east];
console.log(combined2);

//SOAL No. 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
// Driver Code
console.log(before) 
