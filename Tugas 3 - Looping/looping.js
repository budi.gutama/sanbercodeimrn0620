//SanbercodeIMRN0620
//created by budi.gutama 20200617
//tugas 3 - looping

//1. Lopping while
console.log('No. 1 Looping while ');

var sentence_1="I love coding";
var sentence_2="I will become a mobile developer";

//i:nilai awal, n:nilai akhir, jenis:[maju,mundur], gap:penambahan
function whileloop(i, n, jenis='maju', gap=1){
    console.log('loopng '+jenis);
    while (i<=n){
        switch(jenis){
            case 'maju':{ 
                console.log(i+' - '+sentence_1);
                i+=gap; break;
            }    
            case 'mundur':{ 
                console.log(n+' - '+sentence_2);
                n-=gap; break;
            }    
            default :{ 
                console.log(i+' - default');
                i+=1; break;
            }    
        }
        
    }
}

console.log('LOOPING PERTAMA');
whileloop(1,20);
console.log('LOOPING KEDUA');
whileloop(1,15,'mundur',1);
console.log('LOOPING KETIGA');
whileloop(1,15,'mundur',2);


console.log('\n\n');
//2. Looping menggunakan for
console.log('No. 2 Looping menggunakan for');

function getKetegori(n){
    if(n & 1){
        kat="Santai";
        if (n % 3==0){
            kat="I Love Coding";
        }
    }
    else{
        kat ="berkualitas";
    }
    return kat
}

for (i=1; i<=20; i++){
    console.log(i+' - '+getKetegori(i));
}

console.log('\n\n');
//3. Membuat Persegi Panjang 
console.log('No. 3 Membuat Persegi Panjang');

//fungsi create persegi (P:panjang, L:lebar, K:string)
function loopSquare(P, L, K){
    for (i=1; i<=L; i++){
        pjg=K;
        for(n=1;n<=P;n++){
            pjg = pjg.concat(K); 
        }
        console.log(pjg);
    }
}
console.log('persegi 1 #');
loopSquare(12,8,'#');
console.log('persegi 2 Q');
loopSquare(12,6,'Q');


console.log('\n\n');
//4. Membuat Tangga
console.log('No. 4 Membuat Tangga');

//fungsi create tangga (T:Tinggi, K:string)
function loopStairs(T, K){
    pjg=K;
    for(n=1;n<=T;n++){
        console.log(pjg); 
        pjg = pjg.concat(K);
    }
    console.log(pjg); 
}

console.log('Tangga 1 #');
loopStairs(8, '#');
console.log('Tangga 2 X');
loopStairs(12, 'X');

console.log('\n\n');
//5. Membuat Papan Catur
console.log('No. 5 Membuat Papan catur');

//fungsi create persegi (P:panjang, L:lebar, K:string)
function loopChess(P, L, K){
    for (i=1; i<=L; i++){
        if(i & 1){last_K=" ";}
        else {last_K=K;}
        pjg="";
        for(n=1;n<=P;n++){
            if (last_K==K){last_K=" ";}
            else last_K=K;
            pjg = pjg.concat(last_K); 
        }
        console.log(pjg);
    }
}
console.log('papan catur 1 dengan #');
loopCheese(8,8,'#');
console.log('papan catur 2 dengan *');
loopCheese(12,6,'*');

